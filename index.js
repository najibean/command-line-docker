// let http = require('http')
// require('dotenv').config()

// const NAME = process.env.NAME

// http.createServer((req, res) => {
//     res.writeHead(200, {'Content-Type': 'text/html'})
//     res.end(`Hello ${NAME}`)
// }).listen(8092)


const express = require('express');
require('dotenv').config()

// Constants
const PORT = 8092;
const HOST = '0.0.0.0';
const name = process.env.NAME
const mongo_host = process.env.MONGO_HOST
const mongo_port = process.env.MONGO_PORT
const redis_host = process.env.REDIS_HOST
const redis_port = process.env.REDIS_PORT

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello ' + name);
});

app.listen(PORT, HOST);