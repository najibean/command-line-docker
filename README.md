# Belajar Docker

Konten berikut ini adalah intisari command line dari channel youtube **ProgrammerZamanNow** :\
[sumber materi](https://www.youtube.com/playlist?list=PL-CtdCApEFH-A7jBmdertzbeACuQWvQao)


## command line

- melihat semua image yang ada didalam docker\
    `docker image ls` atau `docker images`
- mengambil image dari [docker registry](https://hub.docker.com/search?q=&type=image)\
    `docker pull <nama_registry>:<tag/version>`\
    contoh :
    ```
        docker pull mongo:4.1
    ```
- menghapus image yang ada\
    `docker image rm <nama_image>:<tag/version>`\
    contoh :
    ```
        docker image rm mongo:4.1
    ```
- melihat container yang sedang run\
    `docker container ls`
- melihat semua container yang sudah di create, baik yang sedang run maupun yang tidak\
    `docker container ls -a` atau `docker container ls --all`
- membuat container dari image yang ada\
    `docker container create --name <nama_container> -p <port_on_local>:<port_from_image> <nama_image>:<tag/version>`\
    contoh :
    ```
        docker container create --name mongo-app -p 8080:27017 mongo:4.1
    ```
- membuat container dari image yang ada - Versi 2 (bisa running on the background)\
    `docker run -td --name <nama_container> -p <port_on_local>:<port_from_image> <nama_image>:<tag/version>`\
    contoh :
    ```
        docker run -td --name mongo-app -p 8080:27017 mongo:4.1
    ```
- menjalankan container\
    `docker container start <nama_container>`\
    contoh :
    ```
        docker container start mongo-app
    ```
- memberhentikan container yang sedang run\
    `docker container stop <nama_container>`\
    contoh :
    ```
        docker container stop mongo-app
    ```
- menghapus container\
    `docker container rm <nama_container>`\
    contoh :
    ```
        docker container rm mongo-app   
    ```
- membuat docker image dari aplikasi sendiri\
    `docker build --tag <nama_file>:<tag/version> <dir_file_tsb>`\
    contoh :
    ```
        docker build --tag najib-app:1.0 . 
    ```
    _untuk directory yang sama dengan **Dockerfile** maka cukup dibuat dot ( . )_

- push image yang ada di local komputer ke [docker registry](https://hub.docker.com/search?q=&type=image)\
    `docker tag <nama_file>:<tag/version> <nama_user_dockerHub>/<nama_file>:<tag/version>`\
    contoh :
    ```
        docker tag najib-app:1.0 najibean/najib-app:1.0
    ```
- masuk kedalam container nya\
    `docker exec -it <nama_container> sh` atau `docker exec -it <nama_container> bash`\
    contoh :
    ```
        docker exec -it mongo sh   
    ```

## Troubleshooting

- inspect dari image\
    `docker inspect image <nama_image>`\
    contoh :
    ```
        docker inspect image mongo
    ```
- inspect dari container\
    `docker inspect container <nama_container>`\
    contoh :
    ```
        docker inspect image mongo
    ```
- menampilkan logs dari container\
    `docker container logs <nama_container>`\
    contoh :
    ```
        docker container logs mongo
    ```



  
