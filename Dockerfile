# MEMBUAT IMAGE DOCKER

FROM node:14

# create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install app dependencies
COPY package*.json /usr/src/app
RUN npm config set strict-ssl false
RUN npm install

COPY . /usr/src/app

EXPOSE 8092

CMD ["npm", "start"]